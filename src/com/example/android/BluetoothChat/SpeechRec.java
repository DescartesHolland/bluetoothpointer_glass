package com.example.android.BluetoothChat;

import java.util.ArrayList;

import com.example.bluetoothpointer.R;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

public class SpeechRec extends Activity {
	final static int VOICE_RECOGNITION_REQUEST_CODE = 1234;
	static Context con;
	final static String TAG = "BT_glass"; //debugging
	static ArrayList<String> thingsSaid;
	static boolean stopRecording = false; //record until user swipes down

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("TAG", "onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_speech_rec);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
			.add(R.id.container, new PlaceholderFragment()).commit();
		}

		con = this;
		thingsSaid = new ArrayList<String>();
		
//		ArrayList<String> voiceResults = getIntent().getExtras().getStringArrayList(RecognizerIntent.EXTRA_RESULTS);
//		if (voiceResults != null && voiceResults.size() > 0) {
//			String spokenText = voiceResults.get(0);
//			Log.d(TAG, "000000000000000000"+spokenText);
//		}
	}

	@Override
	public boolean onKeyDown(int keycode, KeyEvent event) {
		if (keycode == KeyEvent.KEYCODE_DPAD_CENTER) {
			stopRecording = false;
			Log.d(TAG, "Click performed");
			Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			i.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
			startActivityForResult(i, VOICE_RECOGNITION_REQUEST_CODE);
		}
		else if(keycode == KeyEvent.KEYCODE_BACK) {
			stopRecording = true;
			finish();
		}
		//        super.onKeyDown(keyCode, event);
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.speech_rec, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_speech_rec,
					container, false);
			return rootView;
		}
	}

	/**
	 * Handle the results from the recognition activity.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult");
		if(!stopRecording) {
			if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
				// Fill the list view with the strings the recognizer thought it could have heard
				thingsSaid.add(data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0));
				Log.d(TAG, thingsSaid.get(thingsSaid.size()-1).toString());
				
				//Check to see if user is stressed:
				String lastThingSaid = thingsSaid.get(thingsSaid.size()-1);
				if(lastThingSaid.startsWith("I'm") && 
						lastThingSaid.endsWith("stressed") ||
						lastThingSaid.endsWith("stressed.")) {
					stopRecording = true;
					//Send chat to handheld:
					
				}
			}
			Log.d(TAG, "Launching new speech recognition");
			onKeyDown(KeyEvent.KEYCODE_DPAD_CENTER, null);
		}
	}

}
