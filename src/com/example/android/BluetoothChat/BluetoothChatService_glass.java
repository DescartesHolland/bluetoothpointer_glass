/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.BluetoothChat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.util.Log;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for
 * incoming connections, a thread for connecting with a device, and a
 * thread for performing data transmissions when connected.
 */
public class BluetoothChatService_glass {
//	public boolean hasBeenConnected = false;

	// Debugging
	private static final String TAG = "BluetoothChatService_glass";
	private static final boolean D = true;

	// Name for the SDP record when creating server socket
	private static final String NAME = "BluetoothChat_glass";

	// Unique UUID for this application
	private static final UUID MY_UUID = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");

	// Member fields
	private final BluetoothAdapter mAdapter;
	private static Handler mHandler;
	private AcceptThread mAcceptThread;
	private ConnectThread mConnectThread;
	public ConnectedThread mConnectedThread;
	private static int mState;

	public boolean QR_RUNNING = false;

	// Constants that indicate the current connection state
	public static final int STATE_NONE = 0;       // we're doing nothing
	public static final int STATE_LISTEN = 1;     // now listening for incoming connections
	public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
	public static final int STATE_CONNECTED = 3;  // now connected to a remote device

	/**
	 * Constructor. Prepares a new BluetoothChat session.
	 * @param context  The UI Activity Context
	 * @param handler  A Handler to send messages back to the UI Activity
	 */
	public BluetoothChatService_glass(Context context, Handler handler) {
		Log.d(TAG, "Instantiating BluetoothChatService_glass");
		mAdapter = BluetoothAdapter.getDefaultAdapter();
		mState = STATE_NONE;
		mHandler = handler;
	}

	/**
	 * Set the current state of the chat connection
	 * @param state  An integer defining the current connection state
	 */
	public synchronized void setState(int state) {
		if (D) Log.d(TAG, "setState() " + mState + " -> " + state);
		mState = state;

		// Give the new state to the Handler so the UI Activity can update
		mHandler.obtainMessage(BluetoothChat_glass.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
	}

	/**
	 * Return the current connection state. */
	public synchronized int getState() {
		return mState;
	}

	/**
	 * Start the chat service. Specifically start AcceptThread to begin a
	 * session in listening (server) mode. Called by the Activity onResume() */
	public synchronized void start() {
		if (D) Log.d(TAG, "start");

		// Cancel any thread attempting to make a connection
		if (mConnectThread != null) {
			Log.d(TAG, "Attempting to cancel mConnectThread and set to null glass");
			mConnectThread.cancel(); mConnectThread = null;
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			Log.d(TAG, "Attempting to cancel mConnectedThread and set to null glass");
			mConnectedThread.cancel(); mConnectedThread = null;
		}

		// Start the thread to listen on a BluetoothServerSocket
		if (mAcceptThread == null) {
			Log.d(TAG, "mAcceptThread null, attempting to instantiate and start a new one glass");
			mAcceptThread = new AcceptThread();
			mAcceptThread.start();
		}
		else {
			Log.d(TAG, "mAcceptThread not null in start(); doing nothing glass");
		}
		setState(STATE_LISTEN);
	}

	/**
	 * Start the ConnectThread to initiate a connection to a remote device.
	 * @param device  The BluetoothDevice to connect
	 */
	public synchronized void connect(BluetoothDevice device) {
		if (D) Log.d(TAG, "connect() to: " + device);

		// Cancel any thread attempting to make a connection
		if (mState == STATE_CONNECTING) {
			Log.d(TAG, "mState is STATE_CONNECTING, cancelling and setting null mConnectThread glass");
			if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			Log.d(TAG, "Cancelling and set to null mConnectedThread glass");
			mConnectedThread.cancel(); mConnectedThread = null;
		}

		// Start the thread to connect with the given device
		Log.d(TAG, "Attempting to create new mConnectThread based on device" + device.getAddress()+" glass");
		mConnectThread = new ConnectThread(device);
		Log.d(TAG, "start()ing mConnectThread");
		mConnectThread.start(); 
		setState(STATE_CONNECTING);
	}

	/**
	 * Start the ConnectedThread to begin managing a Bluetooth connection
	 * @param socket  The BluetoothSocket on which the connection was made
	 * @param device  The BluetoothDevice that has been connected
	 */
	public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
		if (D) Log.d(TAG, "connected( ) to "+device.getAddress()+" with socket "+socket.toString()+" connected "+socket.isConnected()+" glass");
//		hasBeenConnected = true;

		//		if (mConnectThread != null) {
		//			Log.d(TAG, "cancelling mConnectThread in connected( ) and set to null glass");
		//			mConnectThread.cancel();
		//			mConnectThread = null;
		//		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			Log.d(TAG, "cancelling mConnectedThread in connected( ) and set to null glass");
			mConnectedThread.cancel(); mConnectedThread = null;
		}

		// Cancel the accept thread because we only want to connect to one device
		if (mAcceptThread != null) {
			Log.d(TAG, "cancelling mAcceptThread in connected( ) glass"); 
			mAcceptThread.cancel(); mAcceptThread = null;
		}

		// Start the thread to manage the connection and perform transmissions
		Log.d(TAG, "Creating new ConnectedThread in connected( ) glass");
		mConnectedThread = new ConnectedThread(socket);
		Log.d(TAG, "Attempting to start() mConnectedThread");
		mConnectedThread.start();

		// Send the name of the connected device back to the UI Activity
		Message msg = mHandler.obtainMessage(BluetoothChat_glass.MESSAGE_DEVICE_NAME);
		Bundle bundle = new Bundle();
		bundle.putString(BluetoothChat_glass.DEVICE_NAME, device.getName());
		msg.setData(bundle);
		mHandler.sendMessage(msg);

		setState(STATE_CONNECTED);
	}

	/**
	 * Stop all threads
	 */
	public synchronized void stop() {
		if (D) Log.d(TAG, "stop()");
		if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
		if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}
		if (mAcceptThread != null) {mAcceptThread.cancel(); mAcceptThread = null;}
		setState(STATE_NONE);
	}

	/**
	 * Write to the ConnectedThread in an unsynchronized manner
	 * @param out The bytes to write
	 * @see ConnectedThread#write(byte[])
	 */
	public void write(byte[] out) {
		// Create temporary object
		ConnectedThread r;
		// Synchronize a copy of the ConnectedThread
		synchronized (this) {
			if (mState != STATE_CONNECTED) return;
			r = mConnectedThread;
		}
		// Perform the write unsynchronized
		r.write(out);
	}

	/**
	 * Indicate that the connection attempt failed and notify the UI Activity.
	 */
	private void connectionFailed() {
		Log.d(TAG, "connectionFailed() glass");
		setState(STATE_LISTEN);

		// Send a failure message back to the Activity
		Message msg = mHandler.obtainMessage(BluetoothChat_glass.MESSAGE_TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(BluetoothChat_glass.TOAST, "Unable to connect device");
		msg.setData(bundle);
		mHandler.sendMessage(msg);
	}

	/**
	 * Indicate that the connection was lost and notify the UI Activity.
	 */
	private void connectionLost() {
		Log.d(TAG, "connectionLost() glass");
		setState(STATE_LISTEN);

		// Send a failure message back to the Activity
		Message msg = mHandler.obtainMessage(BluetoothChat_glass.MESSAGE_TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(BluetoothChat_glass.TOAST, "Device connection was lost");
		msg.setData(bundle);
		mHandler.sendMessage(msg);
	}

	/**
	 * This thread runs while listening for incoming connections. It behaves
	 * like a server-side client. It runs until a connection is accepted
	 * (or until cancelled).
	 */
	private class AcceptThread extends Thread {
		// The local server socket
		private final BluetoothServerSocket mmServerSocket;

		public AcceptThread() {
			Log.d(TAG, "Instantiating AcceptThread glass");
			Log.d(TAG, "Setting mConnectThread null");
			mConnectThread = null;
			BluetoothServerSocket tmp = null;

			// Create a new listening server socket
			try {
				tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME, MY_UUID);
				Log.d(TAG, "AcceptThread listening glass");
			} catch (IOException e) {
				Log.e(TAG, "AcceptThread listen failed glass", e);
			}
			mmServerSocket = tmp;
		}

		public void run() {
			if (D) Log.d(TAG, "BEGIN mAcceptThread glass" + this);
			setName("AcceptThread");
			BluetoothSocket socket = null;

			// Listen to the server socket if we're not connected
			while (mState != STATE_CONNECTED) {
				Log.d(TAG, "mState not STATE_CONNECTED in AcceptThread glass");
				try {
					// This is a blocking call and will only return on a
					// successful connection or an exception
					Log.d(TAG, "Attempting to call accept() on socket in AcceptThread glass");
					socket = mmServerSocket.accept();
					Log.d(TAG, "accept() WORKED");
				} catch (IOException e) {
					Log.e(TAG, "accept() failed", e);
					break;
				}

				// If a connection was accepted
				if (socket != null) {
					Log.d(TAG, "socket != null (connection accepted?) glass");
					synchronized (BluetoothChatService_glass.this) {
						switch (mState) {
						case STATE_LISTEN:
							Log.d(TAG, "mState is STATE_LISTEN in AcceptThread glass");
						case STATE_CONNECTING:
							// Situation normal. Start the connected thread.
							Log.d(TAG, "mState is STATE_CONNECTING; attempting to connected( )");
							connected(socket, socket.getRemoteDevice());
							break;
						case STATE_NONE:
							Log.d(TAG, "mState is STATE_NONE in AcceptThread glass");
						case STATE_CONNECTED:
							Log.d(TAG, "mState is STATE_CONNECTED (already connected or not ready?) in AcceptThread glass");
							// Either not ready or already connected. Terminate new socket.
							Log.d(TAG, "Terminating socket in AcceptThread glass");
							try {
								Log.d(TAG, "Attempting to close socket because case CONNECTED");
								socket.close();
							} catch (IOException e) {
								Log.e(TAG, "Could not close unwanted socket glass", e);
							}
							break;
						}
					}
				}
			}
			if (D) Log.d(TAG, "END mAcceptThread glass");
		}

		public void cancel() {
			if (D) Log.d(TAG, "cancel() mAcceptThread glass " + this);
			try {
				Log.d(TAG, "Closing mmServerSocket in AcceptThread's cancel() glass");
				mmServerSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of server failed glass", e);
			}
		}
	}

	/**
	 * This thread runs while attempting to make an outgoing connection
	 * with a device. It runs straight through; the connection either
	 * succeeds or fails.
	 */
	public class ConnectThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;

		public ConnectThread(BluetoothDevice device) {
			Log.d(TAG, "Instantiating ConnectThread with device" + device.getAddress() + " glass");
			mmDevice = device;
			BluetoothSocket tmp = null;

			// Get a BluetoothSocket for a connection with the given BluetoothDevice
			try {
				tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
				Log.d(TAG, "ConnectThread created socket glass");
			} catch (IOException e) {
				Log.e(TAG, "create() socket failed; mmSocket will be null", e);
			}
			mmSocket = tmp;
		}

		public void run() {
			Log.i(TAG, "BEGIN mConnectThread glass");
			setName("ConnectThread");

			// Always cancel discovery because it will slow down a connection
			Log.d(TAG, "cancelling discovery glass");
			mAdapter.cancelDiscovery();

			// Make a connection to the BluetoothSocket
			try {
				// This is a blocking call and will only return on a
				// successful connection or an exception
				Log.d(TAG, "Attempting to connect mmSocket in ConnectThread glass");
				mmSocket.connect();
			} catch (IOException e) {
				//				connectionFailed();
				// Close the socket
				try {
					Log.d(TAG, "Attempting to close mmSocket in ConnectThread glass");
					mmSocket.close();
				} catch (IOException e2) {
					Log.e(TAG, "unable to close() socket during connection failure glass", e2);
				}
				// Start the service over to restart listening mode
				Log.d(TAG, "restarting service in glass and calling connectionFailed()");
				BluetoothChatService_glass.this.stop();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				BluetoothChatService_glass.this.start();
				connectionFailed();
				return;
			}

			// Reset the ConnectThread because we're done
			synchronized (BluetoothChatService_glass.this) {
				Log.d(TAG, "mConnectThread is null in glass but shouldn't matter");
				mConnectThread = null;
			}

			// Start the connected thread
			Log.d(TAG, "calling connected( ) from ConnectThread glass");
			connected(mmSocket, mmDevice);
		}

		public void cancel() {
			Log.d(TAG, "cancel() in ConnectThread glass");
			try {
				Log.d(TAG, "Closing mmSocket in connectThread's cancel() glass");
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
		}
	}

	/**
	 * This thread runs during a connection with a remote device.
	 * It handles all incoming and outgoing transmissions.
	 */
	public class ConnectedThread extends Thread {
		public final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;

		public ConnectedThread(BluetoothSocket socket) {
			//assume connected threads always created after accept has happened
			//			mAcceptThread.cancel()d;
			Log.d(TAG, "create ConnectedThread glass");
			mmSocket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			// Get the BluetoothSocket input and output streams
			try {
				Log.d(TAG, "Attempting to get input & output streams glass");
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}
			
			Log.d(TAG, "Results: inAvailable = "+tmpIn.toString()+" "+"out = "+tmpOut.toString());
			mmInStream = tmpIn;
			mmOutStream = tmpOut;
		}

		public void run() {
			Log.d(TAG, "BEGIN mConnectedThread glass");
			byte[] buffer = new byte[1024];
			int bytes;

			// Keep listening to the InputStream while connected
			int cnt = 0;
			while (true && !QR_RUNNING && cnt <= 3) {
				try {
					// Read from the InputStream
					bytes = mmInStream.read(buffer);

					// Send the obtained bytes to the UI Activity
					mHandler.obtainMessage(BluetoothChat_glass.MESSAGE_READ, bytes, -1, buffer)
					.sendToTarget();
				} catch (IOException e) {
					Log.e(TAG, "disconnected glass", e);
					if(mState != STATE_CONNECTED)
						connectionLost();
					else {
						cnt++;
						continue;
					}
					break;
				}
			}
//			if(QR_RUNNING && hasBeenConnected) {
//				Log.d(TAG, "QR not running on glass and hasBeenConnected true");
//				BluetoothChatService_glass.this.closeConnectedThread();
//			}
		}

		/**
		 * Write to the connected OutStream.
		 * @param buffer  The bytes to write
		 */
		public void write(byte[] buffer) {
			try {
				mmOutStream.write(buffer);

				// Share the sent message back to the UI Activity
				mHandler.obtainMessage(BluetoothChat_glass.MESSAGE_WRITE, -1, -1, buffer)
				.sendToTarget();
			} catch (IOException e) {
				Log.e(TAG, "Exception during write", e);
			}
		}

		public void cancel() {
			Log.d(TAG, "cancel() from ConnectedThread glass");
			if (mmInStream != null) {
				Log.d(TAG, "Closing "+this.getClass().getName()+" inputstream from ConnectedThread glass");
				try {mmInStream.close();} catch (Exception e) {}
				//				mmInStream = null;
			}
			if (mmOutStream != null) {
				Log.d(TAG, "Closing "+this.getClass().getName()+" outputstream from ConnectedThread glass");
				try {mmOutStream.close();} catch (Exception e) {}
				//				mmOutStream = null;
			}
			if(mmSocket != null) {
				try {
					Log.d(TAG, "Closing "+this.getClass().getName()+" socket in connectedThread's cancel() glass");
					mmSocket.close();
				} catch (IOException e) {
					Log.e(TAG, "close() of connect socket failed", e);
				}
			}
		}
	}

	public void closeConnectedThread() {
		Log.d(TAG, "closeConnectedThread() (everything) glass");
		try {
			mConnectedThread.mmInStream.close();
			mConnectedThread.mmOutStream.close();
			mConnectedThread.mmSocket.close();
			Log.d(TAG, "Closing mmSocket in closeConnectedThread() glass");
		} catch (IOException  | NullPointerException e) {
			Log.d(TAG, "Couldn't close mConnectedThread's mmSocket");
			e.printStackTrace();
		}
	}
}
