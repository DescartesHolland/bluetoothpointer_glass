package com.example.android.BluetoothChat;

import java.io.ByteArrayInputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bluetoothpointer.R;

/**
 * This is the main Activity that displays the current chat session.
 */
public class BluetoothChat_glass extends Activity {
	// Debugging
	private static final String TAG = "BT_glass";
	private static final boolean D = true;
	Context con;

	// Message types sent from the BluetoothChatService Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// Key names received from the BluetoothChatService Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// Intent request codes
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;

	// Layout Views
	private ImageView mCursorView;

	// Name of the connected device
	private String mConnectedDeviceName = null;
	// String buffer for outgoing messages
	private StringBuffer mOutStringBuffer;
	// Local Bluetooth adapter
	private BluetoothAdapter mBluetoothAdapter = null;
	// Member object for the chat services
	private BluetoothChatService_glass mChatService = null;

	SerializablePoint mPoint = new SerializablePoint();
	Thread mCursorUpdateThread;

	private Handler mUI_Handler = new UIHandler();
	class UIHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			if(D) Log.d(TAG, "Handling Messge");
			mCursorView.setX(mPoint.x);
			mCursorView.setY(mPoint.y);
			super.handleMessage(msg);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*		//Uncomment block to get system screen info.
		 		Display disp = gsetWindowManager().getDefaultDisplay();
				Point p = new Point();
				disp.getSize(p);
				if(D) Log.e(TAG, "Screen Size: "+p.x+" "+p.y);
				DisplayMetrics dm = new DisplayMetrics();
				getWindowManager().getDefaultDisplay().getMetrics(dm);
				if(D) Log.e(TAG, "Screen Size2: "+dm.heightPixels+" "+dm.widthPixels);
		 */
		con = this;

		// Set up the window layout
		setContentView(R.layout.card_pointer);

		// Get local Bluetooth adapter
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
			finish();
			return;
		}

		mCursorUpdateThread = new Thread() {
			public void run() {
				if(D) Log.d(TAG, "cursorUpdateThread running");
				Message msg = Message.obtain(mUI_Handler);
				msg.obj = "new value";
				mUI_Handler.sendMessage(msg);
				return;
			}
		};
	}

	@Override
	public boolean onKeyDown(int keycode, KeyEvent event) {
		if (keycode == KeyEvent.KEYCODE_DPAD_CENTER) {
			try {
				Intent intent = new Intent("com.google.zxing.client.android.SCAN");
				intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes
				startActivityForResult(intent, 0);
				mChatService.QR_RUNNING = true;

			} catch (Exception e) {
				mChatService.start();
				Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
				Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
				startActivity(marketIntent);
			}
		}
		else if(keycode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return true;
	}

	@Override
	public void onStart() {
		super.onStart();
		if(D) Log.e(TAG, "++ ON START glass ++");

		// If BT is not on, request that it be enabled.
		// setupChat() will then be called during onActivityResult
		if ( !mBluetoothAdapter.isEnabled() ) {
			Log.d(TAG, "mBluetoothAdapter disabled, trying to enable it");
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			// Otherwise, setup the chat session
		} else {
			if (mChatService == null) {
				Log.d(TAG, "mBluetoothAdapter enabled, setupChat()");
				setupChat();
			}
		}
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		if(D) Log.e(TAG, "+ ON RESUME glass+");

		// Performing this check in onResume() covers the case in which BT was
		// not enabled during onStart(), so we were paused to enable it...
		// onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
		if (mChatService != null) {
			// Only if the state is STATE_NONE, do we know that we haven't started already
			if (mChatService.getState() == BluetoothChatService_glass.STATE_NONE) {
				Log.d(TAG, "onResume state is STATE_NONE");
				// Start the Bluetooth chat services
				Log.d(TAG, "Starting mChatService because it is null and state_none glass"); 
				mChatService.start();
			}
		}
		else Log.d(TAG, "mChatService is null glass");
	}

	private void setupChat() {
		Log.d(TAG, "setupChat()");

		// Initialize the array adapter for the conversation thread
		//		mConversationArrayAdapter = new ArrayAdapter<String>(this, R.layout.message);
		mCursorView = (ImageView) findViewById(R.id.cursor);
		//		mCursorView.setAdapter(mConversationArrayAdapter);

		// Initialize the BluetoothChatService to perform bluetooth connections
		mChatService = new BluetoothChatService_glass(this, mHandler);

		// Initialize the buffer for outgoing messages
		mOutStringBuffer = new StringBuffer("");
	}

	@Override
	public synchronized void onPause() {
		super.onPause();
		if(D) Log.e(TAG, "- ON PAUSE glass -");
	}

	@Override
	public void onStop() {
		super.onStop();
		if(D) Log.e(TAG, "-- ON STOP glass --");
//		mChatService.stop();
		
//		mChatService.start();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth chat services
		if (mChatService != null) mChatService.stop();
		if(D) Log.e(TAG, "--- ON DESTROY glass ---");
	}

	private void ensureDiscoverable() {
		if(D) Log.d(TAG, "ensure discoverable glass");
		if (mBluetoothAdapter.getScanMode() !=
				BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}

	/**
	 * Sends a message.
	 * @param message  A string of text to send.
	 */
	private void sendMessage(String message) {
		// Check that we're actually connected before trying anything
		if (mChatService.getState() != BluetoothChatService_glass.STATE_CONNECTED) {
			Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
			return;
		}

		// Check that there's actually something to send
		if (message.length() > 0) {
			// Get the message bytes and tell the BluetoothChatService to write
			byte[] send = message.getBytes();
			mChatService.write(send);

			// Reset out string buffer to zero and clear the edit text field
			mOutStringBuffer.setLength(0);
			//            mOutEditText.setText(mOutStringBuffer);
		}
	}

	// The Handler that gets information back from the BluetoothChatService
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
				switch (msg.arg1) {
				case BluetoothChatService_glass.STATE_CONNECTED:
					break;
				case BluetoothChatService_glass.STATE_CONNECTING:
					break;
				case BluetoothChatService_glass.STATE_LISTEN:
					break;
				case BluetoothChatService_glass.STATE_NONE:
					break;
				}
				break;
			case MESSAGE_WRITE:
				byte[] writeBuf = (byte[]) msg.obj;
				// construct a string from the buffer
//				String writeMessage = new String(writeBuf);
				break;
			case MESSAGE_READ:
				byte[] readBuf = (byte[]) msg.obj;
				ByteArrayInputStream bos = new ByteArrayInputStream(readBuf);
				try {
					ObjectInput inObj = new ObjectInputStream(bos);
					SerializablePoint sp = (SerializablePoint) inObj.readObject();
					mPoint.set(sp.x, sp.y);
//					Log.d(TAG, "Setting x,y to "+sp.x+", "+sp.y);
					mCursorView.setX(sp.x);
					mCursorView.setY(sp.y);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case MESSAGE_DEVICE_NAME:
				// save the connected device's name
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				Toast.makeText(getApplicationContext(), "Connected to "
						+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();
				break;
			case MESSAGE_TOAST:
				Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
						Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.scan:
			// Launch the DeviceListActivity to see devices and do scan
			Intent serverIntent = new Intent(this, DeviceListActivity.class);
			startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
			return true;
		case R.id.discoverable:
			// Ensure this device is discoverable by others
			ensureDiscoverable();
			return true;
		}
		return false;
	}

	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(D) Log.d(TAG, "onActivityResult " + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			Log.d(TAG, "Request code CONNECT_DEVICE");
			// When DeviceListActivity returns with a device to connect
			if (resultCode == Activity.RESULT_OK) {
				// Get the device MAC address
				String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				Log.d(TAG, "+++++"+address+"+++++");
				
				// Get the BLuetoothDevice object
				BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
				
				// Attempt to connect to the device
				mChatService.connect(device);
			}
			break;
		case REQUEST_ENABLE_BT:
			Log.d(TAG, "Returned with req to enable BT; may setupChat()"); 
			// When the request to enable Bluetooth returns
			if (resultCode == Activity.RESULT_OK) {
				// Bluetooth is now enabled, so set up a chat session
				setupChat();
			} else {
				// User did not enable Bluetooth or an error occured
				Log.d(TAG, "BT not enabled");
				Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
				finish();
			}
		case 0:
			if(resultCode == Activity.RESULT_OK) {
				Log.d(TAG, "setting QR_RUNNING false in glass");
				mChatService.QR_RUNNING = false;
				
				String contents = data.getStringExtra("SCAN_RESULT");
				Log.d(TAG, "+++Contents:"+contents+"+++");
				Log.d(TAG, "String valid: "+mBluetoothAdapter.checkBluetoothAddress(contents));
				BluetoothDevice dev = mBluetoothAdapter.getRemoteDevice(contents);
				Log.d(TAG, "Attempting to connect to device just created");
				mChatService.connect(dev);
			}
			if(resultCode == RESULT_CANCELED){
				//handle cancel
				Log.d(TAG, "RESULT_CANCELED");
			}
		}
	}
}