package com.example.bluetoothpointer;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.util.Log;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

/**
 * View used to draw a running timer.
 */
public class PointerView extends FrameLayout {
	final static boolean D = true;
	final static String TAG = "POINTER";
    /**
     * Interface to listen for changes on the view layout.
     */
    public interface ChangeListener {
        /** Notified of a change in the view. */
        public void onChange();
    }

    private static final int SOUND_PRIORITY = 1;
    private static final int MAX_STREAMS = 1;
    // Visible for testing.
    static final long DELAY_MILLIS = 1000;

//    private final SoundPool mSoundPool;

    private final ImageView mPointerView;

    private final Handler mHandler = new Handler();
    private final Runnable mUpdatePointerRunnable = new Runnable() {

        @Override
        public void run() {
//            if (mRunning) {
//                postDelayed(mUpdateCursorRunnable, DELAY_MILLIS);
                updatePointer();
//            }
        }
    };

    private final Pointer mPointer;
//    private final Pointer.TimerListener mTimerListener = new Timer.TimerListener() {

//        @Override
//        public void onStart() {
//            mRunning = true;
//            postDelayed(mUpdateCursorRunnable, delayMillis);
//        }
//
//        @Override
//        public void onPause() {
//            mRunning = false;
//            removeCallbacks(mUpdateCursorRunnable);
//        }
//
//        @Override
//        public void onReset() {
//            updatePointer(mPointer.getX(), mPointer.getY());
//        }
//    };
//
//    private boolean mStarted;
//    private boolean mRunning;
//    private boolean mRedText;

    private ChangeListener mChangeListener;

    public PointerView(Context context) {
        this(context, null, 0);
    }

    public PointerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PointerView(Context context, AttributeSet attrs, int style) {
        this(context, attrs, style, new Pointer());
    }

    public PointerView(Context context, AttributeSet attrs, int style, Pointer pointer) {
        super(context, attrs, style);

//        mSoundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0);
//        mPointerFinishedSoundId = mSoundPool.load(context, R.raw.timer_finished, SOUND_PRIORITY);

        LayoutInflater.from(context).inflate(R.layout.card_pointer, this);

        mPointerView = (ImageView) findViewById(R.id.cursor);

        mPointer = pointer;
//        mPointer.setListener(mPointerListener);
        updatePointer(mPointer.getX(), mPointer.getY());
    }

    /** Returns the {@link Timer} model backing up the view. */
    public Pointer getTimer() {
        return mPointer;
    }

    /**
     * Sets a {@link ChangeListener}.
     */
    public void setListener(ChangeListener listener) {
        mChangeListener = listener;
    }

    /**
     * Returns the set {@link ChangeListener}.
     */
    public ChangeListener getListener() {
        return mChangeListener;
    }

    @Override
    public boolean postDelayed(Runnable action, long delayMillis) {
        return mHandler.postDelayed(action, delayMillis);
    }

    @Override
    public boolean removeCallbacks(Runnable action) {
        mHandler.removeCallbacks(action);
        return true;
    }

    /**
     * Updates the text from the Timer's value, overridable for testing.
     */
    public void updatePointer() {
    	if(D) Log.d(TAG, "Updating Pointer no params");
    }

    /**
     * Updates the displayed text with the provided values, overridable for testing.
     */
    public void updatePointer(int newX, int newY) {
    	mPointer.setX(newX);
    	mPointer.setY(newY);
        mPointerView.setX(newX);
        mPointerView.setY(newY);
    	if (mChangeListener != null) {
            mChangeListener.onChange();
        }
    }

    /**
     * Plays the "timer finished" sound once, overridable for testing.
     */
    protected void playSound() {
/*        mSoundPool.play(mPointerFinishedSoundId,
                        1  leftVolume ,
                        1  rightVolume ,
                        SOUND_PRIORITY,
                        0  loop ,
                        1  rate );*/
    }
}
